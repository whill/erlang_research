%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. Sep 2015 1:31 PM
%%%-------------------------------------------------------------------
-module(aCase).
-author("whill").
-compile([debug_info, export_all]).
%% API
-export([]).

createOrExtendSet(X,[])->
  [X];
createOrExtendSet(X,Set)->
  case lists:member(X,Set) of
    true -> Set;
    false -> [X|Set]
  end.

isItniceOut(Temperature)->
  case Temperature of
    {celsius, Temp} when Temp > 19, Temp =< 42 -> go;
    {kelvin, Temp} ->
      if
        Temp >= 292, Temp =< 318 -> go;
        Temp < 292, Temp > 318 -> "Stay Home";
        true -> 'Stay Home'
      end;
    {fahrenheit, Temp} when Temp >= 75, Temp =<90 -> go;
    _ -> "Doesn't look good."
  end.