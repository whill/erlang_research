%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. Sep 2015 2:42 PM
%%%-------------------------------------------------------------------
-module(hexToRGB).
-author("whill").
-import(onePlusOne, [addme/2]).
-compile([debug_info]).
%% API
-export([hexToRGB/1]).

hexToRGB(Hex) ->
  CAA = io_lib:fread("~16u", Hex),
  {_,NumIWant,_} = CAA, %%{OK, [157000], []}
  Vart = hd(NumIWant),
  io:format("~p~n", [binary_to_list(<<Vart:24>>)]),
  io:fwrite("~s~n", [io_lib:write([<<Vart:24>>,<<Vart:32>>])]),

  addme(7,2).
  %%<<Vart:24>>.