%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 05. Oct 2015 2:13 PM
%%%-------------------------------------------------------------------
-module(foldingMapFilters).
-author("whill").
-compile([debug_info, export_all]).
%% API
-export([]).

even(L) -> lists:reverse(even(L, [])).

even([], Acc) -> Acc;
even([H|T], Acc) when H rem 2 =:= 0 ->
  even(T, [H|Acc]);
even([_|T], Acc) ->
  even(T, Acc).

%%-------------------------------------------------------------------
%% Find ages that are over 18
%%-------------------------------------------------------------------

findAdults(Age) -> findAdults(Age, []).

findAdults([], Acc) -> Acc;
findAdults([H|T], Acc) when H > 18 ->
  findAdults(T, [H|Acc]);
findAdults([_|T], Acc) ->
  findAdults(T, Acc).

%%-------------------------------------------------------------------
%% Find people that are over 18 years old
%% [{man, 22}, {woman, 22}, {shark, 22}]
%%-------------------------------------------------------------------

findPeopleOver18(People) -> findPeopleOver18(People, []).

findPeopleOver18([], Acc) -> Acc;
findPeopleOver18([H|T], Acc) ->
  case H of
    {man, Age} = H when Age > 17 -> findPeopleOver18(T, [H|Acc]);
    {woman, Age} = H when Age > 17 -> findPeopleOver18(T, [H|Acc]);
    _ -> findPeopleOver18(T, Acc)
  end.

findPeopleOver18LC(PotentiallyPeople) -> [{Type, Age} || {Type, Age} <- PotentiallyPeople, (Type == man) or (Type == woman) and
  (Age > 17)].

%%-------------------------------------------------------------------
%% Find greatest number
%% [22,17,65,45,33,21,45,76,27,74].
%%-------------------------------------------------------------------
largestNumber(ListOfNumbers) -> largestNumber(ListOfNumbers, []).
largestNumber([], Acc) -> Acc;
largestNumber([H|T], Acc) when H > Acc -> largestNumber(T, H);
largestNumber([_|T], Acc) -> largestNumber(T, Acc).

%%-------------------------------------------------------------------
%% Find smallest number
%% [22,17,65,45,33,21,45,76,27,74].
%%-------------------------------------------------------------------
smallestNumber(ListOfNumbers) -> smallestNumber(ListOfNumbers, []).
smallestNumber([], Acc) -> Acc;
smallestNumber([H|T], Acc) when H < Acc -> smallestNumber(T, H);
smallestNumber([_|T], Acc) -> smallestNumber(T, Acc).

%%-------------------------------------------------------------------
%%  Add items in list - folding. ((((A+B)+C)+D)+E)
%%  [22,17,65,45,33,21,45,76,27,74]
%%  425
%%  >
%%-------------------------------------------------------------------
sumNumbers(ListOfNumbers) -> sumNumbers(ListOfNumbers, 0).
sumNumbers([], Acc) -> Acc;
sumNumbers([H|T], Acc) -> sumNumbers(T, H+Acc).

negate(Number) -> -Number.
%%-------------------------------------------------------------------
%% fold abstraction
%% Apply some function as we're folding list XYZ
%%-------------------------------------------------------------------
foldAbstract(Predicate, SomeList) ->
  [First|Tail] = SomeList,
  foldAbstract(Predicate, First, Tail).
%%-------------------------------------------------------------------
%% Exit i.e. "base case" when 3rd arg is empty list - recursion is over
%%-------------------------------------------------------------------
foldAbstract(_, First, []) -> First;
%%-------------------------------------------------------------------
%% Example of call:
%% foldingMapFilters:foldAbstract(fun(A,B) -> A + B end, [1,2,3,4,5]).
%% 15
%% OR
%% foldingMapFilters:foldAbstract(fun(Head, Acc) -> [Head|Acc] end, [], [1,2,3,4]).
%% First call above needs to hit the 3 arg IMPL for the fold process.
%% This is necessary because we need to start with an empty list vs
%% a new list with the first element in the original list.
%% The recursive operation takes over at this point and concats the head of each
%% successive tail call with the arbitrary list.
%% Second call applies the Predicate defined using the First element and puts it
%% on the head of the new list Acc.
%% The process is repeated until the list is empty and the desired result is returned.
%% OR
%% Write a Predicate that uses another transformer before returning it's mapping service.
%% fun(Head, Result) -> [negate(2 * Head)|Acc] end.
%% foldingMapFilters:foldAbstract(fun(Head, Acc) -> [foldingMapFilters:negate(2 * Head)|Acc] end, [], [1,2,3,4]).
%% [-8,-6,-4,-2]
%% Additional composition:
%% foldingMapFilters:foldAbstract(fun(Head, Acc) -> [Head|Acc] end, [], foldingMapFilters:foldAbstract(fun(Head, Acc) ->
%% [foldingMapFilters:negate(2 * Head)|Acc] end, [], [1,2,3,4])).
%% [-2,-4,-6,-8]
%%-------------------------------------------------------------------
foldAbstract(Predicate, First, [Head|Tail]) ->
  foldAbstract(Predicate, Predicate(Head,First), Tail).