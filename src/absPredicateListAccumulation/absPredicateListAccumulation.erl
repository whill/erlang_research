%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 05. Oct 2015 4:22 PM
%%%-------------------------------------------------------------------
-module(absPredicateListAccumulation).
-author("whill").
-compile([debug_info, export_all]).
%% API
-export([]).

%%-------------------------------------------------------------------
%% Use a Predicate (our filter i.e. assert that something is a: man.
%% Use the usual pattern so far, with a facade to pass an empty accumulator as 3rd arg
%%-------------------------------------------------------------------
absFilter(Predicate, List) -> absFilter(Predicate, List, []).
%%-------------------------------------------------------------------
%% The first signature of the absFilter/3 below deals with an empty list sent to the function - inital call or recursion.
%% The first arg is unimportant in this case (the predicate/assertion) so the _ wildcard is used.
%% The empty accumulator (empty list) is returned.
%%-------------------------------------------------------------------
absFilter(_, [], Acc) -> lists:reverse(Acc);
%%-------------------------------------------------------------------
%% The second signature of the absFilter/3 below deals with an non-empty list sent to the function.
%% The first arg is used as part of the gurad pattern aka the "case".
%% The Head of the list is taken and passed to the predicate/filter.
%% The returned value (true or false assertion) is used to determine if the Head is to be kept.
%% The recursion continues until the list (arg 2) is empty, which results in a call to absFilter/3 above.
%%-------------------------------------------------------------------
absFilter(Predicate, [Head|Tail], Acc) ->
  case Predicate(Head) of
    true -> absFilter(Predicate, Tail, [Head|Acc]);
    false -> absFilter(Predicate, Tail, Acc)
  end.
%%-------------------------------------------------------------------
%% TestFun = fun(X) ->
%%  if X > 3 -> true;
%%    true -> false
%%  end
%% end.
%%
%% absPredicateListAccumulator(TestFun, [1,2,3,4,5]).
%% [4,5]
%%-------------------------------------------------------------------
filterLT3(X) ->
  if
    X > 3 -> true;
    true -> false
  end.
%%-------------------------------------------------------------------
%% Filter eveerything but adult sharks out :)
%% [{man, 22}, {woman, 22}, {shark, 22}]
%%-------------------------------------------------------------------
sharkWeek(ListHeadTuple) ->
  case ListHeadTuple of
    {shark, Age} when Age > 21 -> true;
    _ -> false
  end.