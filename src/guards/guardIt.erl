%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. Sep 2015 10:16 AM
%%%-------------------------------------------------------------------
-module(guardIt).
-author("whill").
-compile([debug_info, export_all]).
%% API
-export([isOldEnough/1]).

isOldEnough(Age) when Age > 21->
  io:format("The patron's (~p) years old, and can go on the ride~n", [Age]);
isOldEnough(Age) when Age < 21 ->
  io:format("The patron is too young (~p) to go on the ride.~n",[Age]).

doIFit(Height,Weight) when Height =< 6.5, Weight =< 100->
  io:format("Hey, good news! You're in :)!~n {Height, (~p)}, {Weight, (~p)}~n",[Height, Weight]);
doIFit(Height,Weight) when Height > 7.0; Weight =< 300->
  io:format("We have a special place for you~n {Height, (~p)}, {Weight, (~p)}~n",[Height, Weight]);
doIFit(_,_)->
  io:format("Nope, you don't fit :(.~n {Height, (~p)}, {Weight, (~p)}~n",["No", "Go"]).