%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. Oct 2015 10:28 AM
%%%-------------------------------------------------------------------
-module(test_calculator).
-author("whill").
-compile([debug_info, export_all]).
-import(calculator,[rpn/1]).

%% API
-export([]).

%%-------------------------------------------------------------------
%% Test function calls
%%-------------------------------------------------------------------
rpn_test() ->
	4 = rpn("2 2 +"),
	18 = rpn("21 3 -"),
	-1 = rpn("1 2 -"),
	ok =	try
					rpn("90 34 12 33 55 66 + * - +")
				catch
					error:{badmatch,[_|_]} -> ok
				end,
	true = rpn("1 1 +") == rpn("4 2 -"),
	false = rpn("1.0 1 +") /= rpn("1 1 +"),
	200.0 = rpn("200 2 / 50 + 50 +"),
	200 = list_to_integer(float_to_list(rpn("200 2 / 50 + 50 +"), [{decimals, 0}])),
	ok.