%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. Oct 2015 8:25 AM
%%%-------------------------------------------------------------------
-module(calculator).
-author("whill").
-compile([debug_info,export_all]).
%% API
-export([]).

%%-------------------------------------------------------------------
%% rpn/1 taks a list (a string in this case, since it's just a list of chars).
%% The format of the string is important - space is the delimiter for token gen into formal list struct.
%% string:tokens("2 d 3", " ") -> ["2","d","3"]
%% The foldl impl takes 3 args. Our rpn/2 definition, an accumulator and our list of tokens.
%% rpn/2 will pattern match against operators, floats and ints.
%% Locked into the recursive operation, ["2","3","+"]
%% rpn("2", []) -> [read("2")|Stack]
%% read("2") -> list_to_integer("2") -> 2 -> stack now = [2]
%% rpn("3", [2]) -> [read("3")|[2]]
%% read("3") -> list_to_integer("3") -> 3 -> stack now = [3,2]
%% rpn("+", [3,2]) -> rpn("+", [3,2|[]]) -> [3 + 2 | []] -> 5
%%-------------------------------------------------------------------
rpn(List) when is_list(List) ->
	[Res] = lists:foldl(fun rpn/2, [], string:tokens(List, " ")),
	Res.

rpn("+", [N1,N2|S]) -> [try N2+N1 catch error:Error -> {error, caught, Error} end|S];
rpn("/", [N1,N2|S]) -> [N2/N1|S];
rpn("*", [N1,N2|S]) -> [N2*N1|S];
rpn("-", [N1,N2|S]) -> [N2-N1|S];
rpn("^", [N1,N2|S]) -> [math:pow(N2, N1)|S];
rpn(Op, Stack) -> [read(Op)|Stack].

read(Op) ->
	case string:to_float(Op) of
		{error,no_float} -> list_to_integer(Op);
		{F,_} -> F
	end.