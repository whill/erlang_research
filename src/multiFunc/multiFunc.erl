%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. Sep 2015 12:57 PM
%%%-------------------------------------------------------------------
-module(multiFunc).
-author("whill").
-compile([debug_info]).

%% API
-export([onlyExportedFunction/0]).

add2(X)->
  2+X.

moduleIsRunningMsg()->
  io:format("multiFunc has been called.~n").

onlyExportedFunction()->
  moduleIsRunningMsg(),
  add2(5).