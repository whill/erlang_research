%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. Oct 2015 2:10 PM
%%%-------------------------------------------------------------------
-module(sendAndKill).
-author("whill").

%% API
-export([loopToKill/0]).

loopToKill() ->
	receive
		3 ->
			io:format("Bang!~n"),
			exit({roulette, die, at, erlang:time()});
		_ ->
			io:format("Click~n"),
		loopToKill()
	end.