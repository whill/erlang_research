%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. Oct 2015 2:36 PM
%%%-------------------------------------------------------------------
-module(watchForDeadOnes).
-author("whill").

%% API
-export([loopUntilDead/0]).

loopUntilDead() ->
	process_flag(trap_exit, true),
	receive
		{monitor, Process} ->
			link(Process),
			io:format("Monitoring process.~n"),
			loopUntilDead();
		{'EXIT', From, Reason} ->
			io:format("Process ~p died because -> ~p", [From, Reason]),
			loopUntilDead()
	end.