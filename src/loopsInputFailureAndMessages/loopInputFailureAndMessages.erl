%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. Oct 2015 10:13 AM
%%%-------------------------------------------------------------------
-module(loopInputFailureAndMessages).
-author("whill").
-compile([debug_info, export_all]).
%% API
-export([]).

loopingFunction() ->
	receive
		"Eric" ->
			io:format("Es Eric en la casa~n"),
			loopingFunction();
		"Ed" ->
			io:format("Es Ed en la casa~n"),
			loopingFunction();
		"Edan" ->
			io:format("Es Edan en la casa~n"),
			loopingFunction();
		"Kristin" ->
			io:format("Es Kristin en la casa~n"),
			loopingFunction();
		"Nitin" ->
			io:format("Es Nitin en la casa~n"),
			loopingFunction();
		"Nozer" ->
			io:format("Es Nozer en la casa~n"),
			loopingFunction();
		"Pavel" ->
			io:format("Es Pavel en la casa~n"),
			loopingFunction();
		_ ->
			io:format("O lip ik :(~n"),
			loopingFunction()
	end.