%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. Oct 2015 2:48 PM
%%%-------------------------------------------------------------------
-module(reiveTheDead).
-author("whill").

%% API
-export([dieAnotherDay/0]).

dieAnotherDay() ->
	process_flag(trap_exit, true),
	receive
		new ->
			io:format("Create and monitor process "),
			register(undead, spawn_link(fun sendAndKill:loopToKill/0)),
			dieAnotherDay();
		{'EXIT', From, Reason} ->
			io:format("The process ~p died because -> ~p", [From, Reason]),
			io:format("Restarting.~n"),
			self() ! new,
			dieAnotherDay()
	end.