%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. Oct 2015 2:10 PM
%%%-------------------------------------------------------------------
-module(sendingSynch).
-author("whill").

%% API
-export([synchLoop/0, translator/2]).

synchLoop() ->
	receive
		{From, _} ->
			From ! "whatever",
			synchLoop()
	end.

translator(Service, Word) ->
	Service ! {self(), Word},
	receive
		SomeTranslation -> SomeTranslation
	end.