%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 05. Oct 2015 10:13 AM
%%%-------------------------------------------------------------------
-module(carryContext).
-author("whill").
-compile([debug_info, export_all]).
%% API
-export([]).

a() ->
  PassWord = "Pony",
  fun() -> PassWord end.

b(F) ->
  "Message carried from another fun: " ++ F().

c() ->
  b(fun a/0).

d() ->
 b(a()).

%%-------------------------------------------------------------------
%% Anon scope
%%-------------------------------------------------------------------

outerFunFail() ->
  A = 1,
  (fun() -> A = 2 end)().

innerFunRedefine() ->
  A = 1,
  (fun(A) -> A = 2 end)(2).

anonFunInternalName() ->
  TimerRef = timer,
  PrepareAlarm = fun(Room) ->
    io:format("Alarm set in ~s.~n", [Room]),
      fun Loop() ->
        io:format("Alarm is active for/in ~s.~n",[Room]),
        TimerRef:sleep(1000),
        TimerRef:kill_after(25000),
        Loop()
      end
    end.