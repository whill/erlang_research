%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 26. Oct 2015 3:10 PM
%%%-------------------------------------------------------------------
-module(shortestPath).
-author("whill").
-compile([debug_info, export_all]).
%% API
-export([]).

main() ->
	File = "/Users/whill/Development/bitbucket/personal/LearnYouSomeErlang/src/bxax/paths.txt",
	{ok, BinRep} = file:read_file(File),
	parseAndMap(BinRep).

groupValues([], Acc) ->
	lists:reverse(Acc);
groupValues([A,B,X|Rest], Acc) ->
	groupValues(Rest, [{A,B,X}|Acc]).

parseAndMap(BinaryRep) when is_binary(BinaryRep) ->
	parseAndMap(binary_to_list(BinaryRep));
parseAndMap(StringRep) when is_list(StringRep) ->
	Values = [list_to_integer(Element) || Element <- string:tokens(StringRep, "\n ")],
	groupValues(Values, []).