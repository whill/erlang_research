%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 01. Oct 2015 11:11 AM
%%%-------------------------------------------------------------------
-module(annonFuncs).
-author("whill").
-compile([debug_info, export_all]).
%% API
-export([]).

%%-------------------------------------------------------------------
%% usage: add(annonFuncs:one/0, annonFuncs:two/0).
%% OR
%% usage: JustOne = fun annonFuncs:one/0. add(JustOne, annonFuncs:two/0).
%%-------------------------------------------------------------------
one() -> 1.
two() -> 2.

add(X,Y) -> X() + Y().

another_factorial(0) -> 1;
another_factorial(N) -> N * another_factorial(N-1).

another_tco_factorial(Number) -> another_tco_factorial(Number, 1).
another_tco_factorial(0, Acc) -> Acc;
another_tco_factorial(Number, Acc) -> another_tco_factorial(Number-1, Number * Acc).

%%Anon func + inner module func call
testFun(a) ->
  Go = fun(X) -> X end,
  Go(one()) * 2;
%%Iteration
testFun(b)->
  Print = fun(X) -> io:format("~p~n", [X]) end,
  lists:foreach(Print, [1,2,3]);
%%-------------------------------------------------------------------
%% Map(F, []) -> F;
%% Map(F, [H|T]) -> [F(H) | map(F, T)].
%%-------------------------------------------------------------------
testFun(c) when c =:= c ->
  Print = fun(X) -> X + 1 end,
  lists:map(Print, [1,2,3,4]);
%% Filtering based on value
testFun(d) ->
  SizeLimit = fun(X) -> X < 3 end,
  lists:filter(SizeLimit, [1,2,3,4,5]);
%% Similiar to JS IMPL of: Array.prototype.every.call([1,2,3], function(x){return x < 4;}) or
%% T = [1,2,3,3], Condition = function(x){return x < 4;}; T.every(Condition). -> true
testFun(e) ->
  TheListToInspect = [1,2,3,4,4,4],
  IncrementBy1 = fun(X) -> X + 1 end,
  LessThan3 = fun(X) -> X < 3 end,
  GreaterThan3 = fun(X) -> X > 3 end,
  Equals3 = fun(X) -> X =:= 3 end,
  PrintEachItem = fun(X) -> io:format("~p", [X]) end,
  io:format("~n~p~nResult of incr each item by 1: ~p~nFilter out anything > 3: ~p~nAny > 3?: ~p~nAll equal to 3?: ~p~n", [
    lists:foreach(PrintEachItem, TheListToInspect),
    lists:map(IncrementBy1, TheListToInspect),
    lists:filter(LessThan3, TheListToInspect),
    lists:any(GreaterThan3, TheListToInspect),
    lists:all(Equals3, TheListToInspect)
  ]).