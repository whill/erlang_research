%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. Sep 2015 10:48 AM
%%%-------------------------------------------------------------------
-module(ifElseTrue).
-author("whill").
-compile([debug_info, export_all]).
%% API
-export([]).

justTestTheNumber(Number) when Number =:= 2->
  WhatToSay = if
                Number =:= 2->
                  "I need to get big, huge!";
                true->"I need to get big, huge!"
  end,
  io:format("What Lulu says all the time ~p~n",[WhatToSay]);
justTestTheNumber(Number)->
  if
    Number =:= 3->
      io:format("It was (~p)~n",[Number]);
    Number =:= 9->
      io:format("It was 3*3 (~p)~n",[Number]);
    true->
      io:format("It (~p) wasn't 3 or 9~n",[Number])
  end,
  5+5.