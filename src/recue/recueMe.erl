%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 30. Sep 2015 5:06 PM
%%%-------------------------------------------------------------------
-module(recueMe).
-author("whill").
-compile([debug_info, export_all]).
%% API
-export([]).


fact(N) -> fact(N, 1).
fact(1, T) -> T;
fact(N, T) -> fact(N-1, T*N).

%%-------------------------------------------------------------------
%% len([1,2,3,4,5].
%% 1 + len([2,3,4,5]).
%% 1 + 1 + len([3,4,5]).
%% 1 + 1 + 1 + len(4,5]).
%% 1 + 1 + 1 + 1 + len([5]).
%% 1 + 1 + 1 + 1 + 1 + len([]).
%% 1 + 1 + 1 + 1 + 1 + 0.
%% 1 + 1 + 1 + 1 + 1
%% 1 + 1 + 1 + 2
%% 1 + 1 + 3
%% 1 + 4
%% 5
%%-------------------------------------------------------------------
len([]) -> 0;
len([_|T]) -> 1 + len(T).

%%-------------------------------------------------------------------
%% lenR([1,2,3,4,5]).
%% lenR([1,2,3,4,5], 0).
%% lenR([2,3,4,5], 1).
%% lenR([3,4,5], 2).
%% lenR([4,5], 3).
%% lenR([5], 4).
%% lenR([], 5).
%% 5
%%-------------------------------------------------------------------
lenR(L) -> lenR(L, 0).
lenR([], Accumulator) -> Accumulator;
lenR([_|T], Accumulator) -> lenR(T, Accumulator + 1).

appendTerm(NumberOfTimes, Term) ->
  appendTerm(NumberOfTimes, Term, []).
appendTerm(0,_,List) ->
  List;
appendTerm(NumberOfTimes, Term, List) when NumberOfTimes > 0 ->
  appendTerm(NumberOfTimes - 1, Term, [Term|List]).

%%-------------------------------------------------------------------
%% Double each element in a list (add to self). Recursive but not Tail Call Optimized.
%%-------------------------------------------------------------------
doubleList([]) -> [];
doubleList([H|T]) -> [H + H | doubleList(T)].
%%-------------------------------------------------------------------
%% Double each element in a list (add to self). Recursive Tail Call Optimized.
%%-------------------------------------------------------------------
doubleListTCO(List) -> doubleListTCO(lists:reverse(List), []).
doubleListTCO([], DBList) -> DBList;
doubleListTCO([H|T], DBList) -> doubleListTCO(T, [H + H|DBList]).
%%doubleListTCO([H|T], DBList) -> doubleListTCO(T, [H|DBList]).
%%-------------------------------------------------------------------
%% Double each element in a list (add to self) when it's an even number.
%%-------------------------------------------------------------------
doubleListComprehension(List) -> [X + X|| X <-List, X rem 2 =:= 0].

reverseAList(List) ->
  reverseAList(List, []).
reverseAList([], ListAccumulator) ->
  ListAccumulator;
reverseAList([H|T], ListAccumulator) ->
  reverseAList(T, [H|ListAccumulator]).

getSubList(List, NumberOfEls) ->
  reverseAList(getSubList(List, NumberOfEls, [])).
getSubList([],_, SubList) -> SubList;
getSubList(_, 0, SubLIst) -> SubLIst;
getSubList([Head|Tail], NumberOfEls, SubList) ->
  getSubList(Tail, NumberOfEls - 1, [Head|SubList]).

%%-------------------------------------------------------------------
%% Sublist Recue
%%-------------------------------------------------------------------
returnSubList(List, Amount) ->
  returnSubList(List, Amount, []).
returnSubList([], _, SubList) -> SubList;
returnSubList(_, 0, SubList) -> SubList;
returnSubList([H|T], Amount, SubList) ->
  returnSubList(T, Amount - 1, [H|SubList]).

%%-------------------------------------------------------------------
%% Zip up two lists with TCO.
%% This is a little more verbose than the example I read.
%% The example uses list construction i.e. [A|ontoSomeAccumulator]. Basically push A to the head of ontoSomeAccumulator.
%% *** Update ***
%% Since the last expression to be evaluated is a function call, the TCO benefit is actually in place as Last Call Optimization.
%% LCO
%% Example -> zip([a,b,c],[1,2,3])
%% zip([a,b,c],[1,2,3], [])
%% zip([b,c],[2,3],[{a,1}|[]])
%% zip([c],[3],[{b,2}|[{a,1}]])
%% zip([],[],[{c,3}|{b,2},[{a,1}]])
%% reverseAList([{c,3},{b,2},[{a,1}])
%% [{a,1},{b,2},{c,3}]
%%-------------------------------------------------------------------
zip(A,B) -> reverseAList(zip(A, B, [])).
zip(_,[],Acc) -> Acc;
zip([],_,Acc) -> Acc;
zip([H|T], [HH|TT], Acc) -> zip(T, TT, [{H, HH}|Acc]).

%%-------------------------------------------------------------------
%% As mentioned above, the list concat version.
%% *** Update ***
%% Since the last expression to be evaluated is a function call, the TCO benefit is actually in place as Last Call Optimization.
%% LCO
%% Example -> zip([a,b,c],[1,2,3])
%% [{a,1}|zip([b,c],[2,3])]
%% [{a,1}, {b,2}|zip([c],[3])]
%% [{a,1}, {b,2}, {c,3}|zip([],[])]
%% [{a,1}, {b,2}, {c,3}]
%%-------------------------------------------------------------------
zipConcat(_,[]) -> [];
zipConcat([],_) -> [];
zipConcat([A|B],[C|D]) -> [{A,C}|zipConcat(B,D)].

%%-------------------------------------------------------------------
%% Sort with pivots
%% Sort [1,4,9,5,6,0,4,3,2,8,7]
%% Starts with the Head of a list i.e. the pivot point.
%% Then the resultant Smaller and Larger lists that are returned within a tuple are pivot sorted again
%% and again, and again until there's nothing left to compare.
%% Then the original pivot is concatenated between the smaller and larger sorted lists.
%%-------------------------------------------------------------------
quicksort([]) -> [];
quicksort([Pivot|Rest]) ->
  {Smaller, Larger} = partition(Pivot,Rest,[],[]),
  quicksort(Smaller) ++ [Pivot] ++ quicksort(Larger).
partition(_,[], Smaller, Larger) -> {Smaller, Larger};
partition(Pivot, [H|T], Smaller, Larger) ->
  if H =< Pivot -> partition(Pivot, T, [H|Smaller], Larger);
    H >  Pivot -> partition(Pivot, T, Smaller, [H|Larger])
  end.