%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 13. Oct 2015 4:56 PM
%%%-------------------------------------------------------------------
-module(tree).
-author("whill").
-compile([debug_info, export_all]).

%% API
-export([empty/0, insert/3]).

%%-------------------------------------------------------------------
%% Root node
%%-------------------------------------------------------------------
empty() -> {node, 'nil'}.

%%-------------------------------------------------------------------
%% Function interleaving root node pattern.
%%-------------------------------------------------------------------
insert(Key, Value, {node, 'nil'}) ->
	{
		node,
		{
			Key, Value,
			{node, nil}, {node, 'nil'}
		}
	};
%%-------------------------------------------------------------------
%% Insert a new key to the left empty spot in the new binary tree
%%-------------------------------------------------------------------
insert(NewKey, NewValue, {node, {Key, Value, Smaller, Larger}}) when NewKey < Key ->
	{
		node,
		{
			Key, Value,
			insert(NewKey, NewValue, Smaller), Larger
		}
	};
%%-------------------------------------------------------------------
%% Insert a new key to the right empty spot in the new binary tree
%%-------------------------------------------------------------------
insert(NewKey, NewValue, {node, {Key, Value, Smaller, Larger}}) when NewKey > Key ->
	{
		node,
		{
			Key, Value, Smaller, insert(NewKey, NewValue, Larger)
		}
	};
insert(Key, Value, {node, {Key, _, Smaller, Larger}}) ->
	{
		node,
		{
			Key, Value, Smaller, Larger
		}
	}.