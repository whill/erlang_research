%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 28. Sep 2015 2:38 PM
%%%-------------------------------------------------------------------
-module(onePlusOne).
-compile([debug_info]).
-author("whill").

%% API
-export([addme/2]).

addme(B, C) -> B + C.


%% blue_eyes 'Blue Eyes'
%% {developer, {language, "Erlang"}}.
%% {"String 1", 2, [{2, 3, 4}, true]}.