%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 12. Oct 2015 2:56 PM
%%%-------------------------------------------------------------------
-module(exceptions).
-author("whill").
-compile([debug_info, export_all]).
%% API
-export([]).

throws(F) ->
  try F() of
    _ -> ok
  catch
    Throw -> {throw, caught, Throw};
    error:Error -> {error, caught, Error};
    exit:Exit -> {exit, caught, Exit}
  end.

errors(F) ->
  try F() of
    _ -> ok
  catch
    error:Error -> {error, caught, Error}
  end.

exits(F) ->
  try F() of
    _ -> ok
  catch
    exit:Exit -> {exit, caught, Exit}
  end.

talk() -> "blah blah".

sword(1) -> throw(slice);
sword(2) -> erlang:error(cut_arm);
sword(3) -> exit(cut_leg);
sword(4) -> throw(punch);
sword(5) -> exit(cross_bridge).

flight(Attack) when is_function(Attack, 0) ->
  try Attack() of
    _ -> "Can't pass"
  catch
    throw:slice -> "Well that hurt";
    error:cut_arm -> "Still have one left";
    exit:cut_leg -> "Can still hop";
    _:_ -> "Hello"
  after
    ok
  end.
%%-------------------------------------------------------------------
%% ^^^^ exceptions:flight(fun() -> exceptions:sword(1) end). ^^^^
%% Wrap in annon func to defer eval until try expression on line 45.
%%-------------------------------------------------------------------

enditWithSideeffects() ->
  try
      talk(),
      _Knight = "Can't go",
      _Doubles = [N*2 || N <- lists:seq(1,100)],
      throw(up),
      _WillReturnThis = tequila
  %% Calls between of and catch can be tail call recursive.
  %% Trying to do tail calls in the protected part will result in mem leak and slow down.
  %% Since the VM keeps a reference in the try block to continually monitor for exceptions.
  of
    tequila -> "warm"
  catch
    Exception:Reason -> {caught, Exception, Reason}
  %% After can't have a return value - one return/function.
  %% It can be used to close files and other connections i.e. cleaning up side effects.
  after
    talk()
  end.

%%-------------------------------------------------------------------
%% Some syntactic sugar for catching exceptions...
%% If you try to divide by zero or a/b -> Exit is thrown.
%% To see tupples thrown for error cases, this can be handy i.e.
%% catch 2/0.
%%   {'EXIT',{badarith,[{erlang,'/',[2,0],[]},
%%   {erl_eval,do_apply,6,[{file,"erl_eval.erl"},{line,673}]},
%%   {erl_eval,expr,5,[{file,"erl_eval.erl"},{line,431}]},
%%   {shell,exprs,7,[{file,"shell.erl"},{line,686}]},
%%   {shell,eval_exprs,7,[{file,"shell.erl"},{line,641}]},
%%   {shell,eval_loop,3,[{file,"shell.erl"},{line,626}]}]}}
%% catch someundef:now().
%%   {'EXIT',{undef,[{someundef,now,[],[]},
%%   {erl_eval,do_apply,6,[{file,"erl_eval.erl"},{line,673}]},
%%   {erl_eval,expr,5,[{file,"erl_eval.erl"},{line,431}]},
%%   {shell,exprs,7,[{file,"shell.erl"},{line,686}]},
%%   {shell,eval_exprs,7,[{file,"shell.erl"},{line,641}]},
%%   {shell,eval_loop,3,[{file,"shell.erl"},{line,626}]}]}}
%%-------------------------------------------------------------------
catcher(X,Y) ->
  case catch X/Y of
    {'EXIT', {badarith,_}} -> "shoot";
    N -> N
  end.