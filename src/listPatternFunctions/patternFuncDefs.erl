%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, LP
%%% @doc
%%%
%%% @end
%%% Created : 29. Sep 2015 3:13 PM
%%%-------------------------------------------------------------------
-module(patternFuncDefs).
-author("whill").
-compile([debug_info]).
%% API
-export([welcome/2]).

welcome(man, Name)->
  io:format("Hello Mr. ~s!", [Name]);
welcome(woman, Name)->
  io:format("Hello Mrs. ~s!\n", [Name]);
welcome(_, Name)->
  io:format("Hello ~s!", [Name]).