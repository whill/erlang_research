%%%-------------------------------------------------------------------
%%% @author whill
%%% @copyright (C) 2015, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 29. Sep 2015 4:10 PM
%%%-------------------------------------------------------------------
-module(listPatternFunctions).
-author("whill").
-compile([debug_info]).

%% API
-export([heads/1, seconds/1, enth/2, timeMatch/1]).

heads([H|_])->H.

seconds([_,S|_])->S.

enth(AList, Nth)->
  lists:nth(AList,Nth).

timeMatch({Date = {Y,M,D}, Time = {H,Min,S}})->
  io:format("The Date tuple sent (~p) was: ~p/~p/~p,~n", [Date, Y, M, D]),
  io:format("The time tuple sent (~p) was: ~p/~p/~p.~n", [Time, H, Min, S]);
timeMatch(_)->
  io:format("The format was not correct.~nExpected{{Y,M,D},{H,M,S}}").